﻿using System;
using System.IO;
using System.Linq;

namespace create_dummy_file
{
    class Program
    {
        static void Main(string[] args)
        {
            var targetDirectory = "";
            var dummyFileName = "";
            if(args.Count() == 1)
            {
                targetDirectory = Directory.GetCurrentDirectory();
                dummyFileName = args[0];
            }
            else if(args.Count() == 2)
            {
                targetDirectory = args[0];
                dummyFileName = args[1];
            }

            if(!Directory.Exists(targetDirectory))
            {
                Console.WriteLine($"\"{targetDirectory}\" does not exist, exiting program ...");
                return;
            }

            Console.WriteLine($"Processing \"{targetDirectory}\" ...");
            CreateEmptyFileOnDirectory(targetDirectory, dummyFileName);
        }

        private static void CreateEmptyFileOnDirectory(string path, string fileName)
        {
            var directories =  Directory.GetDirectories(path);
            var files = Directory.GetFiles(path);
            if(directories.Count() == 0 && files.Count() == 0)
            {
                Console.WriteLine($"Creating dummy file on \"{path}\"");
                using (File.Create(Path.Join(path, fileName))) ;
                return;
            }

            foreach(var directory in directories)
            {
                CreateEmptyFileOnDirectory(directory, fileName);
            }

        }
    }
}
